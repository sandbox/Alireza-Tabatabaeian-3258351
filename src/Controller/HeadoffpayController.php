<?php

namespace Drupal\headoffpay\Controller;

use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\headoffpay\Plugin\Commerce\PaymentGateway\HeadlessOffsitePaymentGatewayInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Decoupled Offsite Payment routes.
 */
class HeadoffpayController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.commerce_payment'),
      $container->get('event_dispatcher')
    );
  }

  public function __construct(LoggerInterface $logger, EventDispatcherInterface $eventDispatcher) {
    $this->logger = $logger;
    $this->eventDispatcher = $eventDispatcher;
  }


  /**
   * Builds the response.
   */
  public function onReturn(Request $request, Order $commerce_order) {

    $payment_gateway = $commerce_order->payment_gateway->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $state = 'Pending';
    if (!$payment_gateway_plugin instanceof HeadlessOffsitePaymentGatewayInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . HeadlessOffsitePaymentGatewayInterface::class);
    }
    try {
      $res = $payment_gateway_plugin->onReturn($commerce_order, $request);
      if ($res->isDone()) {
        $state = 'Ok';
        $payment_manager = \Drupal::entityTypeManager()
          ->getStorage('commerce_payment');
        $payments = $payment_manager->getQuery()
          ->condition('order_id', $commerce_order->id())
          ->condition('state', 'new')
          ->condition('amount__number', $commerce_order->getBalance()
            ->getNumber())
          ->condition('amount__currency_code', $commerce_order->getBalance()
            ->getCurrencyCode())
          ->condition('payment_gateway', $payment_gateway->id())
          ->sort('payment_id', 'DESC')
          ->execute();
        if ($payments) {
          $payment = $payment_manager->load(reset($payments));
          $payment->getState()->applyTransitionById('completed');
          $payment->save();
        }
        else {
          $payment = $payment_manager->create([
              'state' => 'completed',
              'amount' => $commerce_order->getBalance(),
              'payment_gateway' => $payment_gateway->id(),
              'order_id' => $commerce_order->id(),
            ]
          );
          $payment->save();
        }
        if ($commerce_order->getState()->getName() == 'draft') {
          $event = new OrderEvent($commerce_order);
          $this->eventDispatcher->dispatch(CheckoutEvents::COMPLETION, $event);
          $commerce_order->getState()->applyTransitionById('place');
        }
      }
      elseif ($res->isFailed()) {
        $state = 'Failed';
        $payment_manager = \Drupal::entityTypeManager()
          ->getStorage('commerce_payment');
        $payments = $payment_manager->getQuery()
          ->condition('order_id', $commerce_order->id())
          ->condition('state', 'new')
          ->condition('amount__number', $commerce_order->getBalance()
            ->getNumber())
          ->condition('amount__currency_code', $commerce_order->getBalance()
            ->getCurrencyCode())
          ->condition('payment_gateway', $payment_gateway->id())
          ->sort('payment_id', 'DESC')
          ->execute();
        if ($payments) {
          $payment = $payment_manager->load(reset($payments));
          $payment->getState()->applyTransitionById('authorize');
          $payment->getState()->applyTransitionById('void');
          $payment->save();
        }
        else {
          $payment = $payment_manager->create([
              'state' => 'authorization_voided',
              'amount' => $commerce_order->getBalance(),
              'payment_gateway' => $payment_gateway->id(),
              'order_id' => $commerce_order->id(),
            ]
          );
          $payment->save();
        }
      }
    } catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError(t('Payment failed at the payment server. Please review your information and try again.'));
    }


    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Your payment was ' . $state),
    ];

    return $build;
  }

  public function checkAccess() {
    return AccessResult::allowed();
  }

}
