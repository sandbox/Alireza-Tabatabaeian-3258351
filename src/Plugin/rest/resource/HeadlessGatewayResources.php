<?php


namespace Drupal\headoffpay\Plugin\rest\resource;


use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\headoffpay\Services\GetHeadlessGateways;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Resource to get a list of headless gateways
 *
 * @RestResource(
 *   id = "headoffpay_gateway_resource",
 *   label = @Translation("Headless Gateway Resource"),
 *   uri_paths = {
 *     "canonical" = "/headoffpay/get-gateways/{commerce_order}"
 *   }
 * )
 */
class HeadlessGatewayResources extends ResourceBase {

  /**
   * @var GetHeadlessGateways
   */
  protected $headlessGateways;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('headoffpay.get_headless_payments')
    );
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, GetHeadlessGateways $headlessGateways) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->headlessGateways = $headlessGateways;
  }


  /**
   * Responds to entity GET requests.
   *
   * @param Order
   *
   *
   *
   */
  public function get(OrderInterface $commerce_order = NULL) {
    try {
      return new ModifiedResourceResponse($this->headlessGateways->getGateways($commerce_order));
    } catch (InvalidPluginDefinitionException $e) {
      return new ResourceResponse(['message' => $e->getMessage()], 400);
    } catch (PluginNotFoundException $e) {
      return new ResourceResponse(['message' => $e->getMessage()], 404);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);
    $parameters = $route->getOption('parameters') ?: [];
    $parameters['commerce_order']['type'] = 'entity:commerce_order';
    $route->setOption('parameters', $parameters);

    return $route;
  }
}
