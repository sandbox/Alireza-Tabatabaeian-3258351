<?php

namespace Drupal\headoffpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\headoffpay\BankResult\BankResult;
use Drupal\headoffpay\BankResult\BankResultInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the base class for off-site payment gateways.
 */
abstract class HeadlessOffsitePaymentGatewayBase extends PaymentGatewayBase implements HeadlessOffsitePaymentGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) : BankResultInterface {
    return BankResult::Pending();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

}
