<?php

namespace Drupal\headoffpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;

/**
 * Defines the base interface for Headless off-site payment gateways.
 *
 * in special case of headless off-site payment it is assumed that
 * there is no session cause user might be authenticated using token
 * so in this case it can not return to regular onReturn page provided
 * by commerce_payment module.
 *
 * the flow is :
 * 1) user gets a list of all available plugins which support head_off payment method
 * 2) user ask for payment using on of these plugins and will redirected to
 * offsite payment provider (The Bank I mean)
 * 3) after payment provider will be callback our route which then there gateway
 * should tell if payment was SUCCESSFUL, CANCELED or PENDING
 */
interface HeadlessOffsitePaymentGatewayInterface extends PaymentGatewayInterface {

  /**
   * Processes the "return" request.
   *
   * This method should only be concerned with creating/completing payments,
   * the parent order does not need to be touched. The order state is updated
   * automatically when the order is paid in full, or manually by the
   * merchant (via the admin UI).
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\headoffpay\BankResult\BankResult
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the request is invalid or the payment failed.
   */
  public function onReturn(OrderInterface $order, Request $request);

  /**
   * Processes the "cancel" request.
   *
   * Allows the payment gateway to clean up any data added to the $order, set
   * a message for the customer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function onCancel(OrderInterface $order, Request $request);

}
