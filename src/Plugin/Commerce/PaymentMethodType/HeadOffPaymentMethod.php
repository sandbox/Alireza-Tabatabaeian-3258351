<?php


namespace Drupal\headoffpay\Plugin\Commerce\PaymentMethodType;


use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;


/**
 * Provides the debit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "head_off",
 *   label = @Translation("Headless Offsite"),
 * )
 */
class HeadOffPaymentMethod extends PaymentMethodTypeBase {


  public function buildLabel(PaymentMethodInterface $payment_method): string {
    return $this->pluginDefinition['create_label'];
  }

}
