<?php


namespace Drupal\headoffpay\BankResult;


interface BankResultInterface {

  /**
   * Checks whether bank callback was Accepted.
   *
   * @return bool
   *   When TRUE then isFailed() and isPending() are FALSE.
   */
  public function isDone();

  /**
   * Checks whether bank callback yelled Declined.
   *
   * @return bool
   *   When TRUE then isDone() and isPending() are FALSE.
   */
  public function isFailed();

  /**
   * Checks whether there was an error communicating the bank.
   *
   * @return bool
   *   When TRUE then isDone() and isFailed() are FALSE.
   */
  public function isPending();

}
