<?php


namespace Drupal\headoffpay\BankResult;


class BankResult implements BankResultInterface {

  /**
   * @inheritDoc
   */
  public function isDone() {
    return False;
  }

  /**
   * @inheritDoc
   */
  public function isFailed() {
    return False;
  }

  /**
   * @inheritDoc
   */
  public function isPending() {
    return False;
  }

  public static function Done() {
    return new BankResultDone();
  }

  public static function Failed() {
    return new BankResultFailed();
  }

  public static function Pending() {
    return new BankResultPending();
  }
}
