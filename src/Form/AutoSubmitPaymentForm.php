<?php


namespace Drupal\headoffpay\Form;


use Drupal\commerce\InlineFormManager;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AutoSubmitPaymentForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;


  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('entity_type.manager'),
      $container->get('logger.channel.form')
    );
  }

  public function __construct(InlineFormManager $inlineFormManager, EntityTypeManager $entityTypeManager, \Drupal\Core\Logger\LoggerChannel $logger) {
    $this->inlineFormManager = $inlineFormManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'headoffpay_payment_auto_submit';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, Order $commerce_order = NULL, PaymentGateway $commerce_payment_gateway = NULL, $token = '') {
    $commerce_order->set('payment_gateway', $commerce_payment_gateway);
    $commerce_order->set('payment_method', NULL);
    $commerce_order->save();
    $payment = $this->createPayment($commerce_payment_gateway, $commerce_order);
    $inline_form = $this->inlineFormManager->createInstance('payment_gateway_form', [
      'operation' => 'offsite-payment',
      'catch_build_exceptions' => FALSE,
    ], $payment);
    $form['offsite_payment'] = [
      '#parents' => array_merge($form['#parents'], ['offsite_payment']),
      '#inline_form' => $inline_form,
      '#return_url' => $this->buildReturnUrl($commerce_order, $token)->toString(),
      '#cancel_url' => $this->buildCancelUrl($commerce_order)->toString(),
    ];
    try {
      $form['offsite_payment'] = $inline_form->buildInlineForm($form['offsite_payment'], $form_state);
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      $message = $this->t('We encountered an unexpected error processing your payment. Please try again later.');
      $this->messenger()->addError($message);
    }
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing as this form does not submit any thing to Drupal
  }


  protected function createPayment(PaymentGatewayInterface $payment_gateway, Order $order) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $order->getBalance(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $order->id(),
    ]);
    return $payment;
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl(Order $order, $token) {
    return Url::fromRoute('headoffpay.onreturn', [
      'commerce_order' => $order->id(),
      'token' => $token
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl(Order $order) {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

}
