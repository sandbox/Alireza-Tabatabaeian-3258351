<?php


namespace Drupal\headoffpay\Services;


use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Element\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GetHeadlessGateways implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }


  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getGateways(OrderInterface $order): array {
    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    // Load the payment gateways. This fires an event for filtering the
    // available gateways, and then evaluates conditions on all remaining ones.
    $payment_gateways = $payment_gateway_storage->loadMultipleForOrder($order);

    $headoffOptions = [];
    foreach ($payment_gateways as $id => $gateway) {
      $payment_gateway_plugin = $gateway->getPlugin();
      $configuration = $payment_gateway_plugin->getConfiguration();
      $url = \Drupal\Core\Url::fromRoute('headoffpay.callpayment', ['commerce_order' => $order->id(), 'commerce_payment_gateway' => $id, 'token' => $order->uuid()]);

      if(in_array('head_off', $configuration['payment_method_types']))
        $headoffOptions[$id] = [
          'label' => $gateway->label(),
          'url' => $url->toString()
        ];
    }
    return $headoffOptions;
  }

}
